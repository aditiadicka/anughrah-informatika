<?php

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::insert([
            ['key' => 'facebook', 'value' => 'http://facebook.com/you', 'icon' => 'fa fa-facebook'],
            ['key' => 'twitter', 'value' => 'http://twitter.com/you', 'icon' => 'fa fa-twitter'],
            ['key' => 'linkedin', 'value' => 'http://linkedin.com/you', 'icon' => 'fa fa-linkedin'],
            ['key' => 'email', 'value' => 'me@mail.com', 'icon' => 'fa fa-envelope-o'],
            ['key' => 'telp', 'value' => '021-xxxxx', 'icon' => 'fa fa-phone'],
        ]);
    }
}
