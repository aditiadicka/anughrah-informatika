<?php

use App\Models\MasterHero;
use Illuminate\Database\Seeder;

class MasterHeroesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterHero::insert([
            ['key' => 'banner', 'value' => 'img/hero.jpg'],
            ['key' => 'logo', 'value' => 'img/logo.png'],
        ]);
    }
}
