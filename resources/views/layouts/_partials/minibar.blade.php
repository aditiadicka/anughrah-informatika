<div class="top">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <p>Lorem Ipsum Dolor Sit Amet</p>
            </div>
            <div class="col-sm-6">
                <ul class="list-inline pull-right">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-envelope-o"></i></a></li>
                    <li><a href="tel:+902222222222"><i class="fa fa-phone"></i> +90 222 222 22 22</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
