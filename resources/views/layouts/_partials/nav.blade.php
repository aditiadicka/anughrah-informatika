<nav class="navbar navbar-default">
    <div class="container">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar-collapse">
        <span class="sr-only">Toggle Navigation</span>
        <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="bs-navbar-collapse">
            <ul class="nav navbar-nav main-navbar-nav">
                <li class="active"><a href="#profile" title="">Profile</a></li>
                <li class="dropdown">
                    <a href="#" title="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Berita <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="pendidikan.html" title="">Berita Pendidikan</a></li>
                        <li><a href="sekolah.html" title="">Berita Sekolah</a></li>
                    </ul>
                </li>
                <li><a href="data.html" title="">Data Sekolah</a></li>
                <li><a href="event.html" title="">Event Sekolah</a></li>
                <li><a href="footer.html" title="">Kontak Kami</a></li>
            </ul>
        </div>
    </div>
</nav>
