<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SATUAN PELAKSANA PENDIDIKAN KEC. DUREN SAWIT</title>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600&amp;subset=latin-ext" rel="stylesheet">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <header class="site-header">
        @include('layouts._partials.minibar')
        @include('layouts._partials.nav')
    </header>
    <main class="site-main">
        @yield('content')
    </main>
    <!-- Footer -->
    @yield('add_footer')

    <footer class="site-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 fbox">
                    <h4>SATPELDIKCAM DUREN SAWIT</h4>
                    <p class="text">Prima Dalam Pelayanan Unggul Dalam Prestasi. </p>
                    <ul class="list-inline">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-6 fbox">
                    <h4>LINK TERKAIT</h4>
                    <ul class="big">
                        <li><a href="#" title="">DINAS PENDIDIKAN DKI</a></li>
                        <li><a href="#" title="">SUDIN PENDIDIKAN WILAYAH I Jaktim</a></li>
                        <li><a href="#" title="">BKD DKI JAKARTA</a></li>
                        <li><a href="#" title="">Title Four</a></li>
                        <li><a href="#" title="">Title Five</a></li>
                        <li><a href="#" title="">Title Six</a></li>
                        <li><a href="#" title="">Title Seven</a></li>
                        <li><a href="#" title="">Title Eight</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-6 fbox">
                    <h4>CONTENT</h4>
                    <ul class="big">
                        <li><a href="#" title="">Title Satu</a></li>
                        <li><a href="#" title="">Title Dua</a></li>
                        <li><a href="#" title="">Title Tiga</a></li>
                        <li><a href="#" title="">Title Empat</a></li>
                        <li><a href="#" title="">Title Lima</a></li>
                        <li><a href="#" title="">Title Enam</a></li>
                        <li><a href="#" title="">Title Tujuh</a></li>
                        <li><a href="#" title="">Title Delapan</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-6 fbox">
                    <h4>CONTENT</h4>
                    <p class="text">Anugrah Informatika contoh text .</p>
                    <p><a href="tel:+6285648579843"><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span> +62 856 485 79843</a></p>
                    <p><a href="mailto:iletisim@agrisosgb.com"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> mail@awebsitename.com</a></p>
                </div>
            </div>
        </div>
        <div id="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <p class="text-center">&copy; 2017 Built With Anugrah Informatika <i class="fa fa-heart" aria-hidden="true"></i>
                        </p>
                    </div>
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-4">
                        <ul class="list-inline pull-right">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-envelope-o"></i></a></li>
                            <li><a href="tel:+6285648579843"><i class="fa fa-phone"></i> +62 856 485 79843</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- END FOOTER -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $('.carousel[data-type="multi"] .item').each(function(){
          var next = $(this).next();
          if (!next.length) {
            next = $(this).siblings(':first');
          }
          next.children(':first-child').clone().appendTo($(this));

          for (var i=0;i<4;i++) {
            next=next.next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }

            next.children(':first-child').clone().appendTo($(this));
          }
        });
    </script>
</body>
</html>
