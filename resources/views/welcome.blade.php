@extends('layouts.frontend')

@section('content')
<section class="hero_area" style="background-image: url(img/hero.jpg)">
    <div class="hero_content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1>SATUAN PELAKSANA PENDIDIKAN KEC. DUREN SAWIT</h1>
                    <h2>Prima Dalam Pelayanan,<br>  Unggul Dalam Prestasi.</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="services" id="profile">
    <h2 class="section-title">Profile</h2>
    <p class="desc">Profil Satuan Pelaksana Pendidikan KEC Duren Sawit</p>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vitae elementum ante. Nam nibh nibh, maximus sit amet orci quis, ullamcorper fermentum sapien. Etiam eget tempus ante, non tincidunt justo. Sed hendrerit nulla non tempor cursus. Aenean eget tortor congue, viverra mi a, posuere felis. Donec malesuada volutpat nisl sed pharetra. Suspendisse sit amet fringilla lorem, eget convallis mi. Proin eget vehicula ante. Phasellus vel efficitur lorem. Curabitur nec augue sagittis, finibus massa ut, bibendum turpis. Vestibulum quis nisl ut velit dignissim fermentum. Quisque non velit at ante consequat placerat id vitae ligula.
                    Cras vitae sapien ex. Donec lacinia sagittis tortor, sit amet accumsan mauris. Duis a risus viverra, lobortis nibh non, volutpat tortor. Vivamus molestie purus eget sapien dictum, id sollicitudin lacus rutrum. Aenean tincidunt odio id vulputate ultricies. Phasellus scelerisque mi eu purus porttitor, non pharetra elit hendrerit. Sed augue purus, blandit ac dapibus eget, hendrerit venenatis nisi. Cras ac urna semper, dignissim tellus ac, convallis tellus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce pellentesque leo ligula, et vestibulum mi sollicitudin egestas. Phasellus sit amet purus eu nulla venenatis rutrum. Curabitur fermentum nisi faucibus semper placerat.
                    Curabitur eu nunc a nunc accumsan dignissim sit amet a mi. Etiam consequat felis nec quam ultricies, sed laoreet erat blandit. Donec blandit metus justo, at lobortis erat accumsan et. Duis arcu nulla, imperdiet finibus justo sed, auctor dignissim metus. Duis sit amet gravida nisi, eu mattis neque. Vivamus ac ante nec arcu porta congue nec at sapien. Vivamus placerat auctor erat, ac commodo odio faucibus vitae.
                    Vivamus semper, nulla in mollis aliquam, odio massa sollicitudin sapien, quis pulvinar magna felis id metus. Sed maximus ipsum imperdiet, eleifend leo at, maximus diam. Nullam quis nisi maximus, lacinia tellus id, sodales urna. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed tincidunt mauris in lectus lobortis ullamcorper. Praesent augue dui, consequat non maximus eu, consectetur nec magna. Suspendisse bibendum quam augue, sed sollicitudin massa aliquam sed. Sed sed venenatis neque, at pharetra magna.
                    In sagittis dapibus lacus, eget finibus arcu viverra a. Pellentesque egestas semper purus. Sed facilisis est nec enim mattis pellentesque. Donec ut lacus bibendum, luctus velit eu, interdum lacus. Sed feugiat, arcu et pellentesque placerat, tortor lorem porttitor justo, vitae vulputate turpis mauris sed felis. Ut quis eros nisl. Mauris venenatis viverra lectus sed rhoncus. Aenean vel ullamcorper purus, sit amet pulvinar odio. Sed eu tellus quis justo vehicula eleifend ut quis diam. Ut ullamcorper blandit egestas. Etiam dignissim elit nec tortor maximus, in tempor est finibus. Nulla aliquet quam ac nisl efficitur, sit amet gravida nisi fermentum. Aenean nec venenatis nulla, in tempor mi. Nunc quis faucibus odio. Mauris nec lorem pharetra, consectetur risus quis, suscipit metus. Sed eu fermentum nibh, non pretium libero.
                    <br>
                    <h4><b>Visi dan Misi</b></h4>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad aliquam aliquid amet aspernatur consequatur dicta dignissimos ducimus eaque earum et fugiat, fugit iste magni natus, quae ratione recusandae repellat repellendus.
                </p>
            </div>
        </div>
    </div>
</section>

<!-- Berita -->
<section class="home-area">
    <div class="home_content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12"><h2 class="sub_title">Berita Terbaru</h2></div>
                <div class="home_list">
                    <ul>
                        <li class="col-md-3 col-sm-6 col-xs-12">
                            <div class="thumbnail">
                                <img src="img/h1.jpeg" alt="Post">
                                <div class="caption">
                                    <h3><a href="#">Post Title</a></h3>
                                    <p>contoh beritacontoh beritacontoh beritacontoh beritacontoh beritacontoh beritacontoh beritacontoh berita </p>
                                    <a href="#" class="btn btn-link" role="button">More</a>
                                </div>
                            </div>
                        </li>
                        <li class="col-md-3 col-sm-6 col-xs-12">
                            <div class="thumbnail">
                                <img src="img/h2.jpg" alt="Post">
                                <div class="caption">
                                    <h3><a href="#">Post Title</a></h3>
                                    <p>contoh beritacontoh beritacontoh beritacontoh beritacontoh beritacontoh beritacontoh beritacontoh berita </p>
                                    <a href="#" class="btn btn-link" role="button">More</a>
                                </div>
                            </div>
                        </li>
                        <li class="col-md-3 col-sm-6 col-xs-12">
                            <div class="thumbnail">
                                <img src="img/h3.jpeg" class="img-responsive" alt="Post">
                                <div class="caption">
                                    <h3><a href="#">Post Title</a></h3>
                                    <p>contoh beritacontoh beritacontoh beritacontoh beritacontoh beritacontoh beritacontoh beritacontoh berita </p>
                                    <a href="#" class="btn btn-link" role="button">More</a>
                                </div>
                            </div>
                        </li>
                        <li class="col-md-3 col-sm-6 col-xs-12">
                            <div class="thumbnail">
                                <img src="img/h4.jpeg" class="img-responsive" alt="Post">
                                <div class="caption">
                                    <h3><a href="#">Post Title</a></h3>
                                    <p>contoh beritacontoh beritacontoh beritacontoh beritacontoh beritacontoh beritacontoh beritacontoh berita </p>
                                    <a href="#" class="btn btn-link" role="button">More</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END Berita -->
@endsection

@section('add_footer')

    <footer class="site-footer" id="footer">
        <h2 class="section-title">Kontak Kami</h2>
        <p class="text-center">SILAHKAN ISI FORM BERIKUT</p>
        <br>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    TANGAPAN
                </div>

                <div class="col-md-6">
                    <!-- form -->
                    <form>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Name">
                        </div>
                        <div class="form-group">
                           <input type="email" class="form-control" placeholder="Email">
                        </div>
                        <div class="form-group">
                           <input type="text" class="form-control" placeholder="Subject">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" placeholder="message" rows="10" cols="50" required=""></textarea>
                        </div>
                        <div class="form-field">
                             <button class="submitform">Submit</button>
                        </div>
                    </form>
                    <!-- Form End -->
                </div>
            </div>
        </div>
    </footer>
@endsection
