@extends('layouts.admin')

@section('styles')
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
@endsection

@section('content')
    <div class="panel panel-primary">
        <div class="panel-heading text-center">
            Create Berita Sekolah
        </div>

        @include('errors.session_error')

        <div class="panel-body">
            <form action="{{ route('admin.berita-sekolah.store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <div class="col-md-6">
                        <label for="title">Title</label>
                        <input type="text" name="title" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label for="image">Image Cover</label>
                        <input type="file" name="image" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <label for="content">Content</label>
                        <textarea name="content" id="summernote" cols="5" rows="5"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="text-center pull-right">
                            <button class="btn btn-success" type="submit"> Submit </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
@endsection

@section('scripts')
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
    <script>
       $(document).ready(function() {
           $('#summernote').summernote();
       });
    </script>
@endsection
