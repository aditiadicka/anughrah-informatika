@extends('layouts.admin')

@section('styles')
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
@endsection

@section('content')
    <div class="panel panel-primary">
        <div class="panel-heading text-center">
            Create Berita Sekolah
        </div>

        @include('errors.session_error')

        <div class="panel-body">
            <form action="{{ route('admin.berita-sekolah.update', $row->id) }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="form-group">
                    <div class="col-md-6">
                        <label for="title">Title</label>
                        <input type="text" name="title" class="form-control" value="{{ old('title', $row->title) }}">
                    </div>
                    <div class="col-md-6">
                        <label for="image">Image Cover</label>
                        <img src="{{ asset('image/'.$row->image) }}" class="img-responsive" style="max-width: 300px; max-height: 300px">
                        <input type="file" name="image" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <label for="content">Content</label>
                        <textarea name="content" id="summernote" cols="5" rows="5">{{ old('content', $row->content) }}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="text-center pull-right">
                            <button class="btn btn-success" type="submit"> Update </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
@endsection

@section('scripts')
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
    <script>
       $(document).ready(function() {
           $('#summernote').summernote();
       });
    </script>
@endsection
