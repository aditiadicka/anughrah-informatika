@extends('layouts.admin')

@section('content')
    <div class="panel panel-primary">
        <div class="panel-heading text-center">
            Berita Pendidikan
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-bordered table-hovered">
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Image</th>
                    <th>Share social</th>
                    <th colspan="2">Action</th>
                </tr>
                @forelse($rows as $row)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $row->title }}</td>
                    <td><img src="{{ asset('image/'.$row->image) }}" class="img-responsive" style="max-height: 200px; max-width: 200px"></td>
                    <td>
                      <div id="social-links">
                        <a href="https://www.facebook.com/sharer/sharer.php?u={{ config('app.url').'/'.str_slug($row->title) }}" class="social-button btn btn-default btn-xs" id="">
                            <span class="fa fa-facebook"></span>
                        </a>
                        <a href="https://twitter.com/intent/tweet?text=Hello, please take a look.&url={{ config('app.url').'/'.str_slug($row->title) }}" class="social-button btn btn-default btn-xs" id="">
                            <span class="fa fa-twitter"></span>
                        </a>
                        <a href="https://plus.google.com/share?url={{ config('app.url').'/'.str_slug($row->title) }}" class="social-button btn btn-default btn-xs" id="">
                            <span class="fa fa-google-plus"></span>
                        </a>
                        <a href="http://www.linkedin.com/shareArticle?mini=true&url={{ config('app.url').'/'.str_slug($row->title) }}&title=Hello, please take a look.&summary=" class="social-button btn btn-default btn-xs" id="">
                            <span class="fa fa-linkedin"></span>
                        </a>
                      </div>
                    </td>
                    <td>
                        <a href="{{ route('admin.berita-pendidikan.edit', $row->id) }}" class="btn btn-info">Edit</a>
                    </td>
                    <td>
                        <form action="{{ route('admin.berita-pendidikan.destroy', $row->id) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="5">Click <a href="{{ route('admin.berita-pendidikan.create') }}">here</a> to create new berita pendidikan.</td>
                </tr>
                @endforelse
            </table>
        </div>
@endsection
