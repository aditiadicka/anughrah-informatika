@extends('layouts.admin')

@section('styles')
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
@endsection

@section('content')
    <div class="panel panel-primary">
        <div class="panel-heading text-center">
            Edit Form
        </div>

        @include('errors.session_error')

        <div class="panel-body">
            <form action="{{ route('admin.hero.update', $row->id) }}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="form-group">
                    <div class="col-md-6">
                        <label for="value">Image</label>
                        <img src="{{ asset($row->value) }}" class="img-responsive" style="max-width: 300px; max-height: 300px">
                        <input type="file" name="image" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="text-center pull-right">
                            <button class="btn btn-success" type="submit"> Update </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
@endsection

@section('scripts')
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
    <script>
       $(document).ready(function() {
           $('#summernote').summernote();
       });
    </script>
@endsection
