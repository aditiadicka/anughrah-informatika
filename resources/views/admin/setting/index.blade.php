@extends('layouts.admin')

@section('content')
    <div class="panel panel-primary">
        <div class="panel-heading text-center">
            Master Setting
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-bordered table-hovered">
                <tr>
                    <th>#</th>
                    <th>Key</th>
                    <th>Value</th>
                    <th>Action</th>
                </tr>
                @foreach($rows as $row)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $row->key }}</td>
                    <td>{{ $row->value }}</td>
                    <td>
                        <a href="{{ route('admin.setting.edit', $row->id) }}" class="btn btn-info">Edit</a>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
    <div class="panel panel-danger">
        <div class="panel-heading text-center">
            Hero Banner
        </div>
        <div class="panel-body">
            <table class="table table-responsive table-bordered table-hovered">
                <tr>
                    <th>#</th>
                    <th>Key</th>
                    <th>Image</th>
                    <th>Action</th>
                </tr>
                @foreach($heroes as $hero)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $hero->key }}</td>
                    <td><img src="{{ asset($hero->value) }}" class="img-responsive" style="max-height: 300px; max-height: 300px"></td>
                    <td>
                        <a href="{{ route('admin.hero.edit', $hero->id) }}" class="btn btn-info">Edit</a>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection
