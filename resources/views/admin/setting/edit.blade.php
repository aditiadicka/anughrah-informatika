@extends('layouts.admin')

@section('styles')
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
@endsection

@section('content')
    <div class="panel panel-primary">
        <div class="panel-heading text-center">
            Edit Form
        </div>

        @include('errors.session_error')

        <div class="panel-body">
            <form action="{{ route('admin.setting.update', $row->id) }}" method="POST">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="form-group">
                    <div class="col-md-8 col-md-offset-2">
                        <label for="value">Value</label>
                        <input type="text" name="value" class="form-control" value="{{ old('value', $row->value) }}">
                    </div>
                    <br>
                    <button class="btn btn-success" type="submit" style="margin-top: 4px"> Update </button>
                </div>
            </form>
        </div>
@endsection

@section('scripts')
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
    <script>
       $(document).ready(function() {
           $('#summernote').summernote();
       });
    </script>
@endsection
