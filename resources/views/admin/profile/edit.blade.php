@extends('layouts.admin')

@section('styles')
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
@endsection

@section('content')

    @include('admin.partials.error')

    <div class="panel panel-default">

        <div class="panel-heading text-center">

            Edit Profile

        </div>
        <div class="panel-body">
            <form action="{{ route('admin.profile.update',['id' => $content->id]) }}" method="post">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="form-group">
                    <label for="content">Content</label>
                    <textarea name="content" id="summernote" cols="5" rows="5">{{ $content->content }}</textarea>
                </div>
                <div class="form-group">
                    <div class="text-center">
                        <button class="btn btn-success" type="submit"> Update </button>
                    </div>
                </div>
            </form>
        </div>
        @endsection

        @section('scripts')
            <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
            <script>
                $(document).ready(function() {
                    $('#summernote').summernote();
                });
            </script>
@endsection