@extends('layouts.admin')

@section('content')
    <table class="table">
            <thead>
                <tr>
                    <th>Content Profile</th>
                    <th>Action</th>
                </tr>
            </thead>
            @foreach($contents as $content)
                <tbody>
                    <tr>
                        <td>
                            <p>
                                {!! $content->content !!}
                            </p>
                        </td>
                        <td>
                            <a href="{{ route('admin.profile.edit',['id' => $content->id]) }}" class="btn btn-xs btn-info">
                                Edit
                            </a>
                        </td>
                        <td>
                            <form action="{{ route('admin.profile.destroy',['id' => $content->id]) }}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <a class="btn btn-danger btn-xs">Delete</a>
                            </form>
                        </td>
                    </tr>
                </tbody>
            @endforeach
    </table>
@endsection