@extends('layouts.admin')

@section('content')
    <table class="table">
        <thead>
        <tr>
            <th>Event Content</th>
            <th>Action</th>
        </tr>
        </thead>
        @foreach($events as $event)
            <tbody>
            <tr>
                <td>
                    <p>
                        {!! $event->event_content !!}
                    </p>
                </td>
                <td>
                    <a href="{{ route('admin.event.edit', ['id' => $event->id]) }}" class="btn btn-xs btn-info">
                        Edit
                    </a>
                </td>
                <td>
                    <form action="{{ route('admin.event.destroy',['id' => $event->id]) }}" method="post">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <a class="btn btn-danger btn-xs">Delete</a>
                    </form>
                </td>
            </tr>
            </tbody>
        @endforeach

    </table>
    {!! $events->render() !!}


@endsection