@extends('layouts.admin')

@section('styles')
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
@endsection

@section('content')

    @include('admin.partials.error')

    <div class="panel panel-default">

        <div class="panel-heading text-center">

            Create Event Sekolah

        </div>
        <div class="panel-body">
            <form action="{{ route('admin.event.store') }}" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="content">Event Sekolah</label>
                    <textarea name="event_content" id="summernote" cols="5" rows="5"></textarea>
                </div>
                <div class="form-group">
                    <div class="text-center">
                        <button class="btn btn-success" type="submit"> Submit </button>
                    </div>
                </div>
            </form>
        </div>
        @endsection

        @section('scripts')
            <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
            <script>
                $(document).ready(function() {
                    $('#summernote').summernote();
                });
            </script>
@endsection