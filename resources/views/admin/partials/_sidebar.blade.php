<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                        <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
                <!-- /input-group -->
            </li>
            <li>
                <a href="{{ route('home') }}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-wrench fa-fw"></i> Profile<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{ route('admin.profile.index') }}">All Profile</a>
                    </li>
                    <li>
                        <a href="{{ route('admin.profile.create') }}">Create Profile</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="#"><i class="fa fa-wrench fa-fw"></i> Berita Sekolah<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{ route('admin.berita-sekolah.index') }}">All Berita Sekolah</a>
                    </li>
                    <li>
                        <a href="{{ route('admin.berita-sekolah.create') }}">Create Berita Sekolah</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="#"><i class="fa fa-wrench fa-fw"></i> Berita Pendidikan<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{ route('admin.berita-pendidikan.index') }}">All Berita Pendidikan</a>
                    </li>
                    <li>
                        <a href="{{ route('admin.berita-pendidikan.create') }}">Create Berita Pendidikan</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="#"><i class="fa fa-wrench fa-fw"></i> Data Sekolah<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="#">All Data Sekolah</a>
                    </li>
                    <li>
                        <a href="#">Create Data Sekolah</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Event Sekolah<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{ route('admin.event.index') }}">All Event Sekolah</a>
                    </li>
                    <li>
                        <a href="{{ route('admin.event.create') }}">Create Event Sekolah</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="{{ route('admin.setting.index') }}">
                    <i class="fa fa-wrench fa-fw"></i> Setting</span>
                </a>
            </li>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
