<?php

namespace App\Http\Controllers\Admin;

use File;
use Illuminate\Http\Request;
use App\Models\BeritaPendidikan;
use App\Http\Controllers\Controller;

class BeritaPendidikanController extends Controller
{
    public function index()
    {
        $rows = BeritaPendidikan::all();
        return view('admin.beritaPendidikan.index', compact('rows'));
    }

    public function create()
    {
        return view('admin.beritaPendidikan.create');
    }

    public function store()
    {
        $this->validate(request(), [
            'title'     => 'required',
            'image'     => 'required|mimes:jpeg,jpg,png',
            'content'   => 'required',
        ]);

        $row = new BeritaPendidikan;
        $row->title = request('title');

        # Processing Image
        $file       = request()->file('image');
        $fileName   = rand(10000, 99999).'-'.$file->getClientOriginalName();
        request()->file('image')->move('image/', $fileName);

        $row->image = $fileName;
        $row->content = request('content');
        $row->user_id = auth()->user()->id;
        $row->save();

        flash('Create data success.')->success();
        return redirect()->route('admin.berita-pendidikan.index');
    }

    public function edit($id)
    {
        $row = BeritaPendidikan::find($id);
        return view('admin.beritaPendidikan.edit', compact('row'));
    }

    public function update($id)
    {
        $row = BeritaPendidikan::find($id);
        $row->title = request('title');

        # Processing Image
        if(!is_null(request()->file('image'))) {
            $file = 'image/'.$row->image;
            File::delete($file);

            $file       = request()->file('image');
            $fileName   = rand(10000, 99999).'-'.$file->getClientOriginalName();
            request()->file('image')->move('image/', $fileName);

            $row->image = $fileName;
        }

        $row->content = request('content');
        $row->user_id = auth()->user()->id;
        $row->update();

        flash('Update Success.')->success();
        return redirect()->route('admin.berita-pendidikan.index');
    }

    public function destroy($id)
    {
        $row = BeritaPendidikan::find($id);

        # Delete Image
        $file = 'image/'.$row->image;
        File::delete($file);

        $row->delete();
        flash('Delete Success.')->warning();
        return redirect()->route('admin.berita-pendidikan.index');
    }
}
