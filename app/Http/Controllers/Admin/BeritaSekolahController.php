<?php

namespace App\Http\Controllers\Admin;

use File;
use Illuminate\Http\Request;
use App\Models\BeritaSekolah;
use App\Http\Controllers\Controller;

class BeritaSekolahController extends Controller
{
    public function index()
    {
        $rows = BeritaSekolah::all();
        return view('admin.beritaSekolah.index', compact('rows'));
    }

    public function create()
    {
        return view('admin.beritaSekolah.create');
    }

    public function store()
    {
        $this->validate(request(), [
            'title'     => 'required',
            'image'     => 'required|mimes:jpeg,jpg,png',
            'content'   => 'required',
        ]);

        $row = new BeritaSekolah;
        $row->title = request('title');

        # Processing Image
        $file       = request()->file('image');
        $fileName   = rand(10000, 99999).'-'.$file->getClientOriginalName();
        request()->file('image')->move('image/', $fileName);

        $row->image = $fileName;
        $row->content = request('content');
        $row->user_id = auth()->user()->id;
        $row->save();

        flash('Success.')->success();
        return redirect()->route('admin.berita-sekolah.index');
    }

    public function edit($id)
    {
        $row = BeritaSekolah::find($id);
        return view('admin.beritaSekolah.edit', compact('row'));
    }

    public function update($id)
    {
        $row = BeritaSekolah::find($id);
        $row->title = request('title');

        # Processing Image
        if(!is_null(request()->file('image'))) {
            $file = 'image/'.$row->image;
            File::delete($file);

            $file       = request()->file('image');
            $fileName   = rand(10000, 99999).'-'.$file->getClientOriginalName();
            request()->file('image')->move('image/', $fileName);

            $row->image = $fileName;
        }

        $row->content = request('content');
        $row->user_id = auth()->user()->id;
        $row->update();

        flash('Update Success.')->success();
        return redirect()->route('admin.berita-sekolah.index');
    }

    public function destroy($id)
    {
        $row = BeritaSekolah::find($id);

        # Delete Image
        $file = 'image/'.$row->image;
        File::delete($file);

        $row->delete();
        flash('Delete Success.')->warning();
        return redirect()->route('admin.berita-sekolah.index');
    }
}
