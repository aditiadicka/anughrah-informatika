<?php

namespace App\Http\Controllers\Admin;

use App\Models\Profile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $contents = Profile::all();

        return view('admin.profile.index', compact('contents'));
    }

    public function create()
    {
        return view('admin.profile.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'content' => 'required'
        ]);

        $profile = new Profile;
        $profile->content = $request->content;
        $profile->save();

        session()->flash('success', 'Success created a Profile');

        return redirect()->route('admin.profile.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $content = Profile::find($id);

        return view('admin.profile.edit', compact('content'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'content' => 'required'
        ]);

        $content = Profile::find($id);
        $content->content = $request->content;
        $content->save();

        session()->flash('success', 'Success Update Content');

        return redirect()->route('admin.profile.index');
    }

    public function destroy($id)
    {
        $content = Profile::find($id);
        $content->delete();

        return redirect()->route('admin.profile.index');
    }
}
