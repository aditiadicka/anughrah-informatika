<?php

namespace App\Http\Controllers\Admin;

use App\Models\Setting;
use App\Models\MasterHero;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    public function index()
    {
        $rows = Setting::all();
        $heroes = MasterHero::all();
        return view('admin.setting.index', compact('rows', 'heroes'));
    }

    public function edit($id)
    {
        $row = Setting::find($id);
        return view('admin.setting.edit', compact('row'));
    }

    public function update($id)
    {
        $row = Setting::find($id);
        $row->value = request('value');
        $row->save();

        flash('Edit data success.')->success();
        return redirect()->route('admin.setting.index');
    }

    public function heroEdit($id)
    {
        $row = MasterHero::find($id);
        return view('admin.setting.edit-hero', compact('row'));
    }

    public function heroUpdate($id)
    {
        $row = MasterHero::find($id);
        # Processing Image
        if(!is_null(request()->file('image'))) {
            // $file = $row->image;
            // File::delete($file);

            $file       = request()->file('image');
            $fileName   = rand(10000, 99999).'-'.$file->getClientOriginalName();
            request()->file('image')->move('img/', $fileName);

            $row->value = 'img/'.$fileName;
        }
        $row->save();

        flash('Edit data success.')->success();
        return redirect()->route('admin.setting.index');
    }
}
