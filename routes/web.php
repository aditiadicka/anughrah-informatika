<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['namespace' => 'Admin', 'as' => 'admin.', 'prefix' => 'admin'], function () {
    Route::resource('berita-sekolah', 'BeritaSekolahController');
    Route::resource('berita-pendidikan', 'BeritaPendidikanController');
    Route::resource('profile', 'ProfileController');
//    Route::resource('event', 'EventController');

    // Route::get('profile', 'ProfileController')->name('profile.index');
    // Route::get('event', 'EventController')->name('event.test');

    Route::get('setting', 'SettingController@index')->name('setting.index');
    Route::get('setting/{id}/edit', 'SettingController@edit')->name('setting.edit');
    Route::patch('setting/{id}', 'SettingController@update')->name('setting.update');

    Route::get('hero/{id}/edit', 'SettingController@heroEdit')->name('hero.edit');
    Route::patch('hero/{id}', 'SettingController@heroUpdate')->name('hero.update');
});
